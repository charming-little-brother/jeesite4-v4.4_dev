package com.hanzhigu.dangan.service;

import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.hanzhigu.dangan.entity.DanganBase;
import com.hanzhigu.dangan.dao.DanganBaseDao;

/**
 * hr_dangan_baseService
 * @author xiaokang
 * @version 2022-08-04
 */
@Service
@Transactional(readOnly=true)
public class DanganBaseService extends CrudService<DanganBaseDao, DanganBase> {
	
	/**
	 * 获取单条数据
	 * @param danganBase
	 * @return
	 */
	@Override
	public DanganBase get(DanganBase danganBase) {
		return super.get(danganBase);
	}
	
	/**
	 * 查询分页数据
	 * @param danganBase 查询条件
	 * @param danganBase.page 分页对象
	 * @return
	 */
	@Override
	public Page<DanganBase> findPage(DanganBase danganBase) {
		return super.findPage(danganBase);
	}
	
	/**
	 * 查询列表数据
	 * @param danganBase
	 * @return
	 */
	@Override
	public List<DanganBase> findList(DanganBase danganBase) {
		return super.findList(danganBase);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param danganBase
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(DanganBase danganBase) {
		super.save(danganBase);
	}
	
	/**
	 * 更新状态
	 * @param danganBase
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(DanganBase danganBase) {
		super.updateStatus(danganBase);
	}
	
	/**
	 * 删除数据
	 * @param danganBase
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(DanganBase danganBase) {
		super.delete(danganBase);
	}
	
}