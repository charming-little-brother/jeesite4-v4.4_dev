package com.hanzhigu.dangan.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.hanzhigu.dangan.entity.DanganBase;

/**
 * hr_dangan_baseDAO接口
 * @author xiaokang
 * @version 2022-08-04
 */
@MyBatisDao
public interface DanganBaseDao extends CrudDao<DanganBase> {
	
}