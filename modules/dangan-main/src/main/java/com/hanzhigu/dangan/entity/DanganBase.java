package com.hanzhigu.dangan.entity;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;
import com.jeesite.common.mybatis.annotation.JoinTable;
import com.jeesite.common.mybatis.annotation.JoinTable.Type;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * hr_dangan_baseEntity
 * @author xiaokang
 * @version 2022-08-04
 */
@Table(name="hr_dangan_base", alias="a", label="hr_dangan_base信息", columns={
		@Column(name="code", attrName="code", label="关联其它表的字段", isPK=true),
		@Column(name="id", attrName="id", label="流水号"),
		@Column(name="surname", attrName="surname", label="中文姓", queryType=QueryType.LIKE),
		@Column(name="name", attrName="name", label="中文名", queryType=QueryType.LIKE),
		@Column(name="lastname", attrName="lastname", label="英文姓", queryType=QueryType.LIKE),
		@Column(name="firstname", attrName="firstname", label="英文名", queryType=QueryType.LIKE),
		@Column(name="employee_code", attrName="employeeCode", label="员工编码"),
		@Column(name="department", attrName="department", label="所在部门"),
		@Column(name="position", attrName="position", label="职位"),
		@Column(name="marriage_status", attrName="marriageStatus", label="婚姻状态"),
		@Column(name="private_email", attrName="privateEmail", label="私人邮箱"),
		@Column(name="company_email", attrName="companyEmail", label="公司邮箱"),
		@Column(name="phone", attrName="phone", label="手机号码"),
		@Column(name="province", attrName="province", label="省份"),
		@Column(name="city", attrName="city", label="城市"),
		@Column(name="district", attrName="district", label="区域"),
		@Column(name="address", attrName="address", label="详细地址"),
		@Column(name="zip_code", attrName="zipCode", label="邮编"),
		@Column(name="country", attrName="country", label="证件所在国家"),
		@Column(name="document_type", attrName="documentType", label="证件类型"),
		@Column(name="id_number", attrName="idNumber", label="证件号"),
		@Column(name="id_number_start", attrName="idNumberStart", label="证件开始日期", isUpdateForce=true),
		@Column(name="id_number_end", attrName="idNumberEnd", label="证件结束日期", isUpdateForce=true),
		@Column(name="sex", attrName="sex", label="性别"),
		@Column(name="birthday", attrName="birthday", label="出生年月", isUpdateForce=true),
		@Column(name="age", attrName="age", label="年龄"),
		@Column(name="account_address", attrName="accountAddress", label="户口地址"),
		@Column(name="account_zip_code", attrName="accountZipCode", label="户口邮编"),
		@Column(name="account_type", attrName="accountType", label="户口类型"),
		@Column(name="nation", attrName="nation", label="民族"),
		@Column(name="political", attrName="political", label="政治面貌"),
		@Column(name="hobby", attrName="hobby", label="爱好"),
	}, orderBy="a.code DESC"
)
public class DanganBase extends DataEntity<DanganBase> {
	
	private static final long serialVersionUID = 1L;
	private String code;		// 关联其它表的字段
	private String surname;		// 中文姓
	private String name;		// 中文名
	private String lastname;		// 英文姓
	private String firstname;		// 英文名
	private String employeeCode;		// 员工编码
	private String department;		// 所在部门
	private String position;		// 职位
	private String marriageStatus;		// 婚姻状态
	private String privateEmail;		// 私人邮箱
	private String companyEmail;		// 公司邮箱
	private String phone;		// 手机号码
	private String province;		// 省份
	private String city;		// 城市
	private String district;		// 区域
	private String address;		// 详细地址
	private String zipCode;		// 邮编
	private String country;		// 证件所在国家
	private String documentType;		// 证件类型
	private String idNumber;		// 证件号
	private Date idNumberStart;		// 证件开始日期
	private Date idNumberEnd;		// 证件结束日期
	private String sex;		// 性别
	private Date birthday;		// 出生年月
	private String age;		// 年龄
	private String accountAddress;		// 户口地址
	private String accountZipCode;		// 户口邮编
	private String accountType;		// 户口类型
	private String nation;		// 民族
	private String political;		// 政治面貌
	private String hobby;		// 爱好
	
	public DanganBase() {
		this(null);
	}
	
	public DanganBase(String id){
		super(id);
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	@NotBlank(message="中文姓不能为空")
	@Size(min=0, max=255, message="中文姓长度不能超过 255 个字符")
	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	@NotBlank(message="中文名不能为空")
	@Size(min=0, max=255, message="中文名长度不能超过 255 个字符")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@NotBlank(message="英文姓不能为空")
	@Size(min=0, max=255, message="英文姓长度不能超过 255 个字符")
	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	
	@NotBlank(message="英文名不能为空")
	@Size(min=0, max=255, message="英文名长度不能超过 255 个字符")
	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	
	@NotBlank(message="员工编码不能为空")
	@Size(min=0, max=255, message="员工编码长度不能超过 255 个字符")
	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}
	
	@NotBlank(message="所在部门不能为空")
	@Size(min=0, max=64, message="所在部门长度不能超过 64 个字符")
	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}
	
	@NotBlank(message="职位不能为空")
	@Size(min=0, max=64, message="职位长度不能超过 64 个字符")
	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}
	
	@Size(min=0, max=2, message="婚姻状态长度不能超过 2 个字符")
	public String getMarriageStatus() {
		return marriageStatus;
	}

	public void setMarriageStatus(String marriageStatus) {
		this.marriageStatus = marriageStatus;
	}
	
	@Size(min=0, max=64, message="私人邮箱长度不能超过 64 个字符")
	public String getPrivateEmail() {
		return privateEmail;
	}

	public void setPrivateEmail(String privateEmail) {
		this.privateEmail = privateEmail;
	}
	
	@Size(min=0, max=64, message="公司邮箱长度不能超过 64 个字符")
	public String getCompanyEmail() {
		return companyEmail;
	}

	public void setCompanyEmail(String companyEmail) {
		this.companyEmail = companyEmail;
	}
	
	@NotBlank(message="手机号码不能为空")
	@Size(min=0, max=11, message="手机号码长度不能超过 11 个字符")
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	@Size(min=0, max=8, message="省份长度不能超过 8 个字符")
	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}
	
	@Size(min=0, max=8, message="城市长度不能超过 8 个字符")
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	@Size(min=0, max=8, message="区域长度不能超过 8 个字符")
	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}
	
	@Size(min=0, max=255, message="详细地址长度不能超过 255 个字符")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	@Size(min=0, max=6, message="邮编长度不能超过 6 个字符")
	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	
	@Size(min=0, max=255, message="证件所在国家长度不能超过 255 个字符")
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
	@NotBlank(message="证件类型不能为空")
	@Size(min=0, max=64, message="证件类型长度不能超过 64 个字符")
	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
	
	@NotBlank(message="证件号不能为空")
	@Size(min=0, max=18, message="证件号长度不能超过 18 个字符")
	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getIdNumberStart() {
		return idNumberStart;
	}

	public void setIdNumberStart(Date idNumberStart) {
		this.idNumberStart = idNumberStart;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getIdNumberEnd() {
		return idNumberEnd;
	}

	public void setIdNumberEnd(Date idNumberEnd) {
		this.idNumberEnd = idNumberEnd;
	}
	
	@Size(min=0, max=1, message="性别长度不能超过 1 个字符")
	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	
	@Size(min=0, max=255, message="年龄长度不能超过 255 个字符")
	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}
	
	@Size(min=0, max=255, message="户口地址长度不能超过 255 个字符")
	public String getAccountAddress() {
		return accountAddress;
	}

	public void setAccountAddress(String accountAddress) {
		this.accountAddress = accountAddress;
	}
	
	@Size(min=0, max=6, message="户口邮编长度不能超过 6 个字符")
	public String getAccountZipCode() {
		return accountZipCode;
	}

	public void setAccountZipCode(String accountZipCode) {
		this.accountZipCode = accountZipCode;
	}
	
	@Size(min=0, max=255, message="户口类型长度不能超过 255 个字符")
	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	
	@Size(min=0, max=255, message="民族长度不能超过 255 个字符")
	public String getNation() {
		return nation;
	}

	public void setNation(String nation) {
		this.nation = nation;
	}
	
	@Size(min=0, max=4, message="政治面貌长度不能超过 4 个字符")
	public String getPolitical() {
		return political;
	}

	public void setPolitical(String political) {
		this.political = political;
	}
	
	@Size(min=0, max=255, message="爱好长度不能超过 255 个字符")
	public String getHobby() {
		return hobby;
	}

	public void setHobby(String hobby) {
		this.hobby = hobby;
	}
	
}