package com.hanzhigu.dangan.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.hanzhigu.dangan.entity.DanganBase;
import com.hanzhigu.dangan.service.DanganBaseService;

/**
 * hr_dangan_baseController
 * @author xiaokang
 * @version 2022-08-04
 */
@Controller
@RequestMapping(value = "${adminPath}/dangan/danganBase")
public class DanganBaseController extends BaseController {

	@Autowired
	private DanganBaseService danganBaseService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public DanganBase get(String code, boolean isNewRecord) {
		return danganBaseService.get(code, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("dangan:danganBase:view")
	@RequestMapping(value = {"list", ""})
	public String list(DanganBase danganBase, Model model) {
		model.addAttribute("danganBase", danganBase);
		return "hanzhigu/dangan/danganBaseList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("dangan:danganBase:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<DanganBase> listData(DanganBase danganBase, HttpServletRequest request, HttpServletResponse response) {
		danganBase.setPage(new Page<>(request, response));
		Page<DanganBase> page = danganBaseService.findPage(danganBase);
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("dangan:danganBase:view")
	@RequestMapping(value = "form")
	public String form(DanganBase danganBase, Model model) {
		model.addAttribute("danganBase", danganBase);
		return "hanzhigu/dangan/danganBaseForm";
	}

	/**
	 * 保存数据
	 */
	@RequiresPermissions("dangan:danganBase:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated DanganBase danganBase) {
		danganBaseService.save(danganBase);
		return renderResult(Global.TRUE, text("保存hr_dangan_base成功！"));
	}
	
	/**
	 * 删除数据
	 */
	@RequiresPermissions("dangan:danganBase:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(DanganBase danganBase) {
		danganBaseService.delete(danganBase);
		return renderResult(Global.TRUE, text("删除hr_dangan_base成功！"));
	}
	
}